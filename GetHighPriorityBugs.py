#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import urllib.request
import re
from datetime import date
from lxml import html

import BlenderChatSendMessage

modules = {
    "Animation & Rigging" : "https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=285%2c268%2c-297%2c-298%2c-299%2c-301&milestone=0&project=0&assignee=0&poster=0",
    "Core" : "https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=285%2c269%2c-297%2c-298%2c-299%2c-301&milestone=0&project=0&assignee=0&poster=0",
    "Grease Pencil" : "https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=285%2c273%2c-297%2c-298%2c-299%2c-301&milestone=0&project=0&assignee=0&poster=0",
    "Modeling" : "https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=285%2c274%2c-297%2c-298%2c-299%2c-301&milestone=0&project=0&assignee=0&poster=0",
    "Nodes & Physics" : "https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=285%2c275%2c-298%2c-299%2c-297%2c-301&milestone=0&project=0&assignee=0&poster=0",
    "Pipeline, Assets & I/O" : "https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=285%2c276%2c-297%2c-298%2c-299%2c-301&milestone=0&project=0&assignee=0&poster=0",
    "Platforms, Builds, Test & Devices" : "https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=285%2c278%2c-297%2c-298%2c-299%2c-301&milestone=0&project=0&assignee=0&poster=0",
    "Python API" : "https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=285%2c279%2c-301%2c-299%2c-298%2c-297&milestone=0&project=0&assignee=0&poster=0",
    "Render & Cycles" : "https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=285%2c280%2c-301%2c-299%2c-298%2c-297&milestone=0&project=0&assignee=0&poster=0",
    "EEVEE & Viewport" : "https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=285%2c272%2c-297%2c-298%2c-299%2c-301&milestone=0&project=0&assignee=0&poster=0",
    "Sculpt, Paint & Texture" : "https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=285%2c281%2c-297%2c-298%2c-299%2c-301&milestone=0&project=0&assignee=0&poster=0",
    "User Interface" : "https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=285%2c283%2c-297%2c-298%2c-299%2c-301&milestone=0&project=0&assignee=0&poster=0",
    "VFX & Video" : "https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=285%2c284%2c-297%2c-298%2c-299%2c-301&milestone=0&project=0&assignee=0&poster=0",
    "Total" : "https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=285%2c-297%2c-298%2c-299%2c-301&milestone=0&project=0&assignee=0&poster=0",
    "Unbreak Now!": "https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=288&milestone=0&project=0&assignee=0&poster=0"
}


def get_source(url):
    source = urllib.request.urlopen(url)
    source_bytes = source.read()
    source.close()

    return source_bytes.decode("utf8")


def get_number_of_bugs(source):  
    tree = html.fromstring(source)
    items = tree.xpath('//li[contains(@class, "item")]')
    return len(items)


# Get number of all bugs from modules and assemble a summary for the chat
report = "Open High Priority bugs as of %s:\n\n" % date.today()

for module in modules:
    bugs = get_number_of_bugs(get_source(modules[module]))
    if (module == "Total") or (module == "Unbreak Now!"):
        report += ("\n[%s](%s): %s\n" % (module, modules[module], bugs))
    else:
        report += ("- [%s](%s): %s\n" % (module, modules[module], bugs))

BlenderChatSendMessage.login()
BlenderChatSendMessage.sendMessage(report)