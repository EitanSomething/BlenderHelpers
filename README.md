# BlenderHelpers

Script for getting the number of high-priority bugs in the blender repo and sending a message to #blender-coders on blender.chat with that number.